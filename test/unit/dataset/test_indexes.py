from pandas_dataset import Dataset
from test_dataset import setup

def test_indexes_1():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    indexes = {
        "df1": [[0], [1], [2], [3]],
        "df1-df2": [[0], [1, 3], [2, 4], [5]]
    }
    assert data.indexes == indexes

def test_indexes_2():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert data._indexes is None
    assert data.indexes == {
        "df1": [[0], [1], [2], [3]],
        "df1-df2": [[0], [1, 3], [2, 4], [5]]
    }
    assert data._indexes is not None
