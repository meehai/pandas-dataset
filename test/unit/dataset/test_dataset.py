from typing import Iterable

import numpy as np
import pandas as pd

from pandas_dataset import Dataset

def setup():
    df0 = pd.DataFrame({
        "cid": [1, 2, 3, 4, 5],
        "val": [1, 4, 9, 16, 25]
    }).set_index("cid")
    df1 = pd.DataFrame({
        "cid": [1, 2, 3, 4],
        "val": [1, 4, 9, 16]
    }).set_index("cid")
    df2 = pd.DataFrame({
        "cid": [1, 2, 3, 2, 3, 4],
        "sid": [1, 2, 3, 4, 5, 6],
        "val2": [1, 4, 9, 16, 25, 36]
    }).set_index("sid")
    df3 = np.array([1, 2, 3, 4, 5])
    return df0, df1, df2, df3

def test_init_ok():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2}, check_consistency=True)
    assert isinstance(data["df1"], pd.DataFrame)
    assert isinstance(data["df2"], pd.DataFrame)
    assert data.primary_keys == {"df1": "cid", "df2": "sid"}

def test_series_values_to_df_ok():
    df0, df1, df2, df3 = setup()
    ser_1 = df1["val"]
    assert isinstance(ser_1, pd.Series)
    data = Dataset({"df1": ser_1, "df2": df2})
    assert isinstance(data["df1"], pd.DataFrame)
    assert isinstance(data["df2"], pd.DataFrame)

def test_series_values_to_df_error():
    df0, df1, df2, df3 = setup()
    data = {"df1": df3, "df2": df2}
    try:
        _ = Dataset(data)
        raise Exception
    except ValueError:
        pass

def test_sort_keys_ok():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert data.tables == ["df1", "df2"]
    data = Dataset({"df2": df2, "df1": df1})
    assert data.tables == ["df1", "df2"]

def test_validate_inner_join_consistency_ok():
    df0, df1, df2, df3 = setup()
    _ = Dataset({"df1": df1, "df2": df2}, check_consistency=True)

def test_validate_inner_join_consistency_error():
    df0, df1, df2, df3 = setup()
    data = {"df1": df1, "df2": df2}
    dataset = Dataset(data)
    assert dataset.data_groups == [ ["df1", "df2"] ]
    data["df2"].drop(columns=["cid"], inplace=True)
    # creating the dataset and then modifying it from outside will lead to corrupted data groups
    try:
        dataset.check_consistency()
        raise Exception
    except KeyError:
        pass
    dataset2 = Dataset(data)
    assert dataset2.data_groups == [ ["df1"], ["df2"] ]

def test_validate_inner_join_consistency_value_error():
    df0, df1, df2, df3 = setup()
    data = {"df1": df0, "df2": df2}
    try:
        _ = Dataset(data, check_consistency=True)
        raise Exception
    except ValueError:
        pass

def test_validate_inner_join_consistency_nans_error():
    df0, df1, df2, df3 = setup()
    df_new = pd.DataFrame({
        "cid": [1, 2, 3, 4],
        "val": [1, 4, 9, np.nan]
    }).set_index("cid")
    data = {"df1": df_new, "df2": df2}
    try:
        _ = Dataset(data, check_consistency=True)
        raise Exception
    except ValueError:
        pass

def test_data():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert data.data == {"df1": df1, "df2": df2}

def test_join_keys():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    join_keys = {"df1": [], "df2": ["cid"]}
    assert data.join_keys == join_keys

def test_join_keys_2():
    df0, df1, df2, df3 = setup()
    df3 = pd.DataFrame({
        "cid": [1, 1, 2, 2, 3, 3, 2, 3, 4],
        "sid": [1, 2, 2, 3, 3, 4, 4, 5, 6],
        "hid": [1, 2, 3, 9, 8, 6, 7, 4, 5],
        "val3": [0, 1, 8, 27, 64, 125, 216, 343, 512],
    }).set_index("hid")
    data = Dataset({"df1": df1, "df2": df2, "df3": df3})
    join_keys = {"df1": [], "df2": ["cid"], "df3": ["cid", "sid"]}
    assert data.join_keys == join_keys

def test_primary_keys():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    p_keys = {"df1": "cid", "df2": "sid"}
    assert data.primary_keys == p_keys

def test_columns():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    cols = {"df1": ["val"], "df2": ["cid", "val2"]}
    assert data.columns == cols

def test_all_column_names():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    colnames = ["val", "cid", "sid", "val2"]
    assert set(data.all_column_names) == set(colnames)

def test_shape():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    shape = {"df1": (4, 1), "df2": (6, 2)}
    assert data.shape == shape

def test_sort():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    df1 = pd.DataFrame({
        "cid": [1, 3, 2, 4],
        "val": [1, 9, 4, 16]
    }).set_index("cid")
    df2 = pd.DataFrame({
        "sid": [1, 2, 3, 4, 6, 5],
        "cid": [1, 2, 3, 2, 4, 3],
        "val2": [1, 4, 9, 16, 36, 25]
    }).set_index("sid")
    unsorted_data = Dataset({"df1": df1, "df2": df2})
    sorted_data = unsorted_data.sort(inplace=False)
    assert data != sorted_data
    assert (sorted_data["df1"].index == [1, 3, 2, 4]).all()

def test_get_column():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert (data.get_column("val")[1] - df1["val"]).sum() == 0
    try:
        _ = data.get_column(col_name="no_way")
        raise Exception
    except KeyError:
        pass

def test_merge_same_keys():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    df1 = pd.DataFrame({
        "cid": [1, 2, 3, 4, 5],
        "new_val": [1, 4, 9, 16, 25]
    }).set_index("cid")
    df2 = pd.DataFrame({
        "cid": [1, 2, 3, 2, 3, 4],
        "sid": [1, 2, 3, 4, 5, 6],
        "new_val2": [1, 4, 9, 16, 25, 36]
    }).set_index("sid")
    new_data = Dataset({"df1": df1, "df2": df2})
    merged_data = data.merge(new_data)
    assert set(merged_data.all_column_names) == {"val", "new_val", "cid", "sid", "val2", "new_val2"}
    assert merged_data["df1"].shape == (4, 2)
    assert merged_data["df2"].shape == (6, 3)

def test_merge_diff_keys():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1})
    new_data = Dataset({"df2": df2})
    merged_data = data.merge(new_data)
    assert merged_data == Dataset({"df1": df1, "df2": df2})

def test_merge_key_error():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    df1 = pd.DataFrame({
        "cid": [1, 2, 3, 4, 5],
        "new_val": [1, 4, 9, 16, 25]
    }).set_index("cid")
    df2 = pd.DataFrame({
        "cid": [1, 2, 3, 2, 3, 4],
        "new_id": [1, 2, 3, 4, 5, 6],
        "new_val2": [1, 4, 9, 16, 25, 36]
    }).set_index("new_id")
    new_data = Dataset({"df1": df1, "df2": df2})
    try:
        _ = data.merge(new_data)
        raise Exception
    except KeyError:
        pass

def test_getitem_1():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert isinstance(data["df1"], pd.DataFrame)
    assert data["df1"].shape == df1.shape
    index = {"df1": ["val"], "df2": ["val2", "cid"]}
    assert isinstance(data[index], Dataset)
    assert data[index]["df1"].shape == (4, 1)
    assert data[index]["df2"].shape == (6, 2)

def test_getitem_2():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert data["df1"].shape == df1.shape
    assert data[["df1", "df2"]] == data
    assert data[["df1", "df2"]]["df2"].shape == data["df2"].shape
    try:
        _ = data[["df1"]]["df2"]
        raise Exception
    except KeyError:
        pass

def test_setitem():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    try:
        data["df3"] = df1
        raise Exception
    except ValueError:
        pass
        _ = data.tables

def test_contains():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert "df1" in data
    assert "df3" not in data

def test_equals():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert data == data
    assert data != {"df1": df1, "df2": df2}
    assert data == Dataset({"df1": df1, "df2": df2})

    new_data = Dataset({"df1": df1.copy(), "df2": df2.copy()})
    new_data["df1"]["val"].iloc[0] = 199
    assert data != new_data
    new_data = Dataset({"df1": df1.copy(), "df2": df2.copy()})
    assert data == new_data
    assert data == data.copy()

def test_iter():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert isinstance(iter(data), Iterable)

def test_str():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert "df1" in str(data)
    assert "df2" in str(data)

def test_repr():
    df0, df1, df2, df3 = setup()
    data = Dataset({"df1": df1, "df2": df2})
    assert str(data) == repr(data)

def test_get_data_shape_1():
    data = {
        "dg1": pd.DataFrame(np.random.randn(5, 10), columns=range(10)).set_index(0),
        "dg2": pd.DataFrame(np.random.randn(10, 100), columns=[0, *range(10, 109)]).set_index(10),
        "dg3": pd.DataFrame(np.random.randn(15, 30), columns=[0, 10, *range(109, 137)]).set_index(109),
    }
    shapes = Dataset(data).shape
    assert shapes["dg1"] == (5, 9) and shapes["dg2"] == (10, 99) and shapes["dg3"] == (15, 29)

def test_modify_underlying_dataframe():
    df = pd.Series(np.random.randint(0, 10, size=1000), name="col").to_frame()
    dataset = Dataset({"dg1": df})
    dataset2 = Dataset({"dg1": df})
    assert dataset == dataset2

    # explicit copy of the dataframe
    dataset3 = dataset.copy()
    assert dataset == dataset3

    # Modifying any dataset inplace will affect all items owning the underlying dataframe (df, dataset2 & dataset2)
    dataset["dg1"]["col"].iloc[0:5] *= 0
    dataset["dg1"]["col"].iloc[0:5] += 100
    assert dataset == dataset2
    assert (df["col"].iloc[0:5] == 100).all()

    # Modifying the df inplace will affect all items owning the underlying dataframe (df, dataset2 & dataset2)
    df["col"].iloc[0:5] = 33
    assert dataset == dataset2
    assert (dataset["dg1"]["col"].iloc[0:5] == 33).all()

    # However, explicitly copying the dataframe will create a deepcopy of all the owned dataframes
    assert dataset != dataset3

def test_invalid_tables_cycle():
    data = {
        "dg1": pd.DataFrame(np.random.randn(5, 1), columns=["A"]).set_index("A"),
        "dg2": pd.DataFrame(np.random.randn(5, 2), columns=["A", "B"]).set_index("B"),
        "dg3": pd.DataFrame(np.random.randn(5, 4), columns=["A", "B", "C", "D"]).set_index("C"),
        "dg4": pd.DataFrame(np.random.randn(5, 4), columns=["A", "B", "C", "D"]).set_index("D"),
    }
    try:
        _ = Dataset(data)
        raise Exception
    except ValueError:
        pass

def test_getattribute_table_1():
    df0, df1, df2, df3 = setup()
    data = {"df1": df1, "df2": df2}
    dataset = Dataset(data)
    assert dataset.df1.index.name == "cid"
    assert dataset.df2.index.name == "sid"
    assert (dataset["df1"] == dataset.df1).all().all()
    assert (dataset["df2"] == dataset.df2).all().all()

def test_dataframe_index_name_null_autorename():
    df = pd.Series(np.random.randint(0, 10, size=1000), name="col").to_frame()
    assert df.index.name is None
    dataset = Dataset({"dg1": df})
    assert df.index.name == "dg1"
    assert dataset["dg1"].index.name == "dg1"

def test_same_column_names_across_tables():
    df1 = pd.DataFrame({
        "cid": [1, 2, 3, 4, 5],
        "new_val": [1, 4, 9, 16, 25]
    }).set_index("cid")
    df2 = pd.DataFrame({
        "cid": [1, 2, 3, 2, 3, 4],
        "sid": [1, 2, 3, 4, 5, 6],
        "new_val": [1, 4, 9, 16, 25, 36]
    }).set_index("sid")

    try:
        _ = Dataset({"df1": df1, "df2": df2})
        raise Exception
    except ValueError:
        pass

if __name__ == "__main__":
    test_sort()
