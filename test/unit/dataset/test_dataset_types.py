import pandas as pd
import numpy as np
from pandas_dataset import Dataset
from natsort import natsorted

def test_dataset_types_categorical_1():
    df = pd.Series(np.random.randint(0, 10, size=1000), name="col").to_frame()

    # categorical columns must be of type pd.CategoricalDType
    try:
        _ = Dataset({"dg1": df}, {"dg1": {"col": "categorical"}})
        raise Exception
    except TypeError:
        pass

    categories = natsorted(df["col"].unique())
    df["col"] = pd.Categorical(df["col"], categories=categories)
    dataset2 = Dataset({"dg1": df}, {"dg1": {"col": "categorical"}})

    # cateogricals can also be autodetected from the pandas type
    dataset3 = Dataset({"dg1": df})
    assert dataset2 == dataset3
