from common import build_dataset
from pandas_dataset import TorchDataset

def test_pandas_dataset():
    dataset = build_dataset(n_root=100, n_nested=300)
    assert dataset.shape == {"root": (100, 2), "nested": (300, 2)}

def test_torch_dataset():
    base_dataset = build_dataset(n_root=100, n_nested=300)
    dataset = TorchDataset(base_dataset)
    assert dataset.data_shape == {"root": (100, 1), "nested": (300, 20)}
    x = dataset[0]
    assert x["root"].shape == (1, ) and len(sh := x["nested"].shape) == 2 and sh[-1] == 20 # variable number in nested

    x_slice = dataset[0:5]
    assert x_slice["root"].shape == (5, 1) and len(x_slice["nested"]) == 5
    assert all(y.shape[-1] == 20 for y in x["nested"])

