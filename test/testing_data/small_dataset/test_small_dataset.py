from __future__ import annotations
import numpy as np
import ast
from functools import lru_cache
from pandas_dataset import Dataset
from pathlib import Path

def attributes_converter(x: list | str) -> list:
    """
        Apply ast.literal_eval to convert value to list. Only used for product attributes in CSV mode when we can
        receive both lists of strings ors trings. Do not use it in any other code except converting attributes to
        lists of strings.
    Args:
        x: The attribute column to be converted
    Returns:
        The converted attribute column.
    """
    if isinstance(x, list):
        return x
    assert isinstance(x, str), x
    if x.startswith("[") or x.startswith("("):
        return ast.literal_eval(x)
    return [x]

@lru_cache
def read_data():
    data_root = Path(__file__).parent.absolute()
    object_converters = {
        "hit_product_skus": lambda row: np.array(ast.literal_eval(row), dtype=object),
        "hit_shopping_stages": lambda row: np.array(ast.literal_eval(row), dtype=object),
        "attribute": attributes_converter,
    }
    dataset = Dataset.read(data_root, data_format="csv", object_converters=object_converters,
                           tables_pk={"users": "client_id", "sessions": "session_id", "products": "product_sku"})
    return dataset

def test_small_dataset_exists():
    dataset = read_data()
    assert dataset["users"].shape == (3, 0)
    assert dataset["sessions"].shape == (7, 5)
    assert dataset["products"].shape == (5, 1)
    assert dataset["products"].attribute.dtype == object
    assert isinstance(dataset["products"].attribute.iloc[0], list)
    assert len(dataset["products"].attribute.iloc[0]) == 2
