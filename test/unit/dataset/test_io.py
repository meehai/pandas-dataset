from pathlib import Path
import numpy as np
from tempfile import TemporaryDirectory
from pandas_dataset import Dataset
from pandas_dataset.utils import get_project_root
from test_dataset import setup

data_root = get_project_root() / "test/testing_data"

def test_read_csv_1():
    dataset = Dataset.read(data_root / "small_dataset", "csv",
                           tables_pk={"users": "client_id", "sessions": "session_id"})
    assert dataset.tables == ["users", "sessions"]
    assert len(dataset["sessions"]) == 7
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).shape == (7, 16)
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).sum() == 0

def test_read_csv_2():
    dataset = Dataset.read(data_root / "small_dataset", "csv", tables_pk={"sessions": "session_id"})
    assert dataset.tables == ["sessions"]
    assert len(dataset["sessions"]) == 7
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).shape == (7, 16)
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).sum() == 0

def test_read_csv_3():
    dataset = Dataset.read(data_root / "fake_client_data", "csv",
                           tables_pk={"users": "client_id", "sessions": "session_id"})
    assert dataset.tables == ["users", "sessions"]
    assert len(dataset["users"]) == 1000
    assert len(dataset["sessions"]) == 8359

def test_read_csv_4():
    dataset = Dataset.read(data_root / "small_dataset", "csv",
                           tables_pk={"users": "client_id", "sessions": "session_id", "products": "product_sku"})
    assert dataset.data_groups == [["products"], ["users", "sessions"]]
    assert dataset.tables == ["products", "users", "sessions"]
    assert len(dataset["sessions"]) == 7
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).shape == (7, 16)
    assert np.stack(dataset["sessions"]["session_affinities_attr_3"]).sum() == 0

def test_read_write_csv_1():
    tables_pk = {"users": "client_id", "sessions": "session_id"}
    dataset = Dataset.read(data_root / "fake_client_data", "csv", tables_pk=tables_pk)
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv")
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=tables_pk)
    assert dataset == dataset2

def test_peek_csv_1():
    tables_pk = {"users": "client_id", "sessions": "session_id"}
    header = Dataset.peek(data_root / "fake_client_data", "csv", tables_pk=tables_pk)
    expected = {
        "users": {
            "client_id": "str", "total_number_of_sessions": "int", "browser": "categorical",
            "operating_system": "categorical", "device_category": "categorical",
            "mobile_device_branding": "categorical", "mobile_device_model": "categorical", "city": "categorical",
            "country": "categorical", "user_revenue": "float"
        },
        "sessions": {
            "session_id": "str", "hit_count": "int", "hit_shopping_stages": "object", "hit_product_skus": "object",
            "client_id": "str", "session_bounces": "int", "session_date_hour_minute": "datetime64[ns]",
            "session_duration": "float", "session_transaction_revenue": "float"
        }
    }
    assert sorted(header, key=lambda kv: kv[0]) == sorted(expected)

def test_peek_csv_2():
    tables_pk = {"users": "client_id"}
    header = Dataset.peek(data_root / "fake_client_data", "csv", tables_pk=tables_pk)
    assert header == {
        "users": {
            "total_number_of_sessions": "int", "browser": "categorical", "operating_system": "categorical",
            "device_category": "categorical", "mobile_device_branding": "categorical",
            "mobile_device_model": "categorical", "city": "categorical", "country": "categorical",
            "user_revenue": "float", "client_id": "str"
        },
    }

def test_read_csv_vector_equality_1():
    dgpk = {"users": "client_id", "sessions": "session_id"}
    dataset = Dataset.read(data_root / "small_dataset", "csv", tables_pk=dgpk)
    dataset2 = Dataset.read(data_root / "small_dataset", "csv", tables_pk=dgpk)
    assert dataset == dataset2

def test_read_csv_vector_equality_2():
    dgpk = {"users": "client_id", "sessions": "session_id"}
    dataset = Dataset.read(data_root / "small_dataset", "csv", tables_pk=dgpk)
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv")
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=dgpk)
    assert dataset == dataset2

def test_write_indexes_1():
    df0, df1, df2, df3 = setup()
    dataset = Dataset({"df1": df1, "df2": df2})
    assert dataset._indexes is None
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv", write_indexes=False)
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=dataset.primary_keys)
    assert dataset2._indexes is None

def test_write_indexes_2():
    df0, df1, df2, df3 = setup()
    dataset = Dataset({"df1": df1, "df2": df2})
    _ = dataset.indexes
    assert dataset._indexes is not None
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv", write_indexes=False)
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=dataset.primary_keys)
    assert dataset2._indexes is None
    assert dataset.indexes == dataset2.indexes

def test_write_indexes_3():
    df0, df1, df2, df3 = setup()
    dataset = Dataset({"df1": df1, "df2": df2})
    _ = dataset.indexes
    assert dataset._indexes is not None
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv", write_indexes=True)
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=dataset.primary_keys)
    assert dataset2._indexes is not None
    assert dataset.indexes == dataset2.indexes

def test_write_indexes_4():
    dataset = Dataset.read(data_root / "small_dataset", "csv",
                           tables_pk={"users": "client_id", "sessions": "session_id", "products": "product_sku"})
    tempdir = Path(TemporaryDirectory().name)
    dataset.write(tempdir, "csv", write_indexes=True)
    dataset2 = Dataset.read(tempdir, "csv", tables_pk=dataset.primary_keys)
    assert dataset2._indexes is not None
    assert dataset.indexes == dataset2.indexes

if __name__ == "__main__":
    test_read_csv_4()
