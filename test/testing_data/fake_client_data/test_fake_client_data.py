from __future__ import annotations
from pathlib import Path
from functools import lru_cache
import ast
import numpy as np

from pandas_dataset import Dataset
from pandas_dataset.internal import flatten_list

def attributes_converter(x: list | str) -> list:
    """
        Apply ast.literal_eval to convert value to list. Only used for product attributes in CSV mode when we can
        receive both lists of strings ors trings. Do not use it in any other code except converting attributes to
        lists of strings.
    Args:
        x: The attribute column to be converted
    Returns:
        The converted attribute column.
    """
    if isinstance(x, list):
        return x
    assert isinstance(x, str), x
    if x.startswith("[") or x.startswith("("):
        return ast.literal_eval(x)
    return [x]

@lru_cache
def read_data():
    data_root = Path(__file__).parent.absolute()
    object_converters = {
        "hit_product_skus": lambda row: np.array(ast.literal_eval(row), dtype=object),
        "hit_shopping_stages": lambda row: np.array(ast.literal_eval(row), dtype=object),
        "attr_1": attributes_converter,
        "attr_2": attributes_converter,
        "attr_3": attributes_converter,
    }
    dataset = Dataset.read(data_root, data_format="csv", object_converters=object_converters,
                           tables_pk={"users": "client_id", "sessions": "session_id", "products": "product_sku"})

    return dataset


def test_fake_client_data_files_exist():
    dataset = read_data()
    assert dataset.tables == ["products", "users", "sessions"]
    assert dataset.data_groups == [["products"], ["users", "sessions"]]

def test_fake_client_data_primary_key_ordered():
    sessions = read_data()["sessions"]
    # we sort the users by [client_id, date] and check that the session ids are indeed properly sorted by date
    # for each user.
    sorted_sessions_by_date = sessions.reset_index().groupby("client_id") \
        .apply(lambda x: x["session_date_hour_minute"].sort_values()) \
        .reset_index()
    assert (sorted_sessions_by_date["level_1"] == range(len(sessions))).all()

def test_fake_client_data_sessions_with_revenue_have_transactions():
    sessions = read_data()["sessions"]
    transaction_sessions = sessions["hit_shopping_stages"].apply(lambda row: "TRANSACTION" in row)
    rev_sessions = (sessions["session_transaction_revenue"] > 0)
    assert (rev_sessions == transaction_sessions).all()

def test_products_and_interactions_match():
    # we test that all the products in the "hit_product_skus" column exist in the products dataset
    dataset = read_data()

    sessions = dataset["sessions"].copy()
    flatten_skus = sessions["hit_product_skus"].apply(lambda x: flatten_list(x.tolist()))
    all_interactions_skus = set(flatten_list(flatten_skus.values.tolist()))
    diffs = list(all_interactions_skus.difference(dataset["products"].index))
    assert len(diffs) == 0
