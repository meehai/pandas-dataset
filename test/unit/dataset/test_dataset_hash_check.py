import pandas as pd
import numpy as np
from pandas_dataset import Dataset, DatasetHashCheck

def test_dataset_hash_check():
    df = pd.DataFrame({
        "a": [1, 2, 3, 4, 5],
        "b": [1, 4, 9, 16, 25]
    })
    dataset = Dataset({"df": df})
    with DatasetHashCheck(dataset):
        dataset["df"]["b"].apply(np.sqrt)
    check = DatasetHashCheck(dataset)

    dataset["df"]["a"] = dataset["df"]["b"].apply(np.sqrt).astype(int)
    try:
        check()
    except ValueError:
        pass

    dataset["df"]["a"] = dataset["df"]["b"].apply(np.sqrt)
    try:
        check()
        assert False
    except ValueError:
        pass
