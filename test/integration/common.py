import pandas as pd
import numpy as np
from pandas_dataset import Dataset

def build_dataset(n_root: int, n_nested: int) -> Dataset:
    df_root = pd.DataFrame({
        "column": np.random.randn(n_root,), # random floats
        "column2": [''.join(map(chr, y)) for y in np.random.randint(ord("A"), ord("z"), size=(n_root, 10))] # text
    }).set_index(pd.Index(range(n_root), name="root_index"))
    df_nested = pd.DataFrame({
        "column3": [x.astype(object) for x in np.random.randn(300, 20)], # vector column (embeddings)
        "root_index": np.random.randint(0, 100, size=(n_nested, )) # join key with df_root
    })
    return Dataset({"root": df_root, "nested": df_nested})
