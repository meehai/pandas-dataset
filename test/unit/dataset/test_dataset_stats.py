import pandas as pd
import numpy as np
from pandas_dataset import Dataset
from natsort import natsorted

def test_dataset_stats_1():
    chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'i', 'j', 'k']
    df = pd.DataFrame([
        np.random.randint(0, 2, size=1000).astype(bool),
        np.random.randint(0, 10, size=1000),
        np.random.random(size=1000) * 100 - 50,
        np.array(chars)[np.random.randint(0, 10, size=1000)],
    ], index=["col_bool", "col_int", "col_float", "col_str"]).T
    df["col_bool"] = df["col_bool"].astype(bool)
    df["col_int"] = df["col_int"].astype(int)
    df["col_float"] = df["col_float"].astype(float)
    dataset = Dataset({"dg": df}, column_types={"dg": {"col_bool": "bool", "col_int": "int",
                                                     "col_float": "float", "col_str": "str"}})
    stats = dataset.stats["dg"]
    assert np.allclose(stats["col_bool"], [0, 1])
    assert np.allclose(stats["col_int"], [0, 9])
    assert len(stats["col_float"]) == 2 and stats["col_float"][0] >= -50 and stats["col_float"][1] <= 50
    assert stats["col_str"] is None
